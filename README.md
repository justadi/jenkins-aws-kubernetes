# Jenkins - AWS - Kubernetes on macOS

> How to set up a local environment with minikube for testing a CI/CD pipeline.

### Preperation - Minikube / Kubernetes

Let's install minikube on macOS so we could use Kubernetes locally.

`brew install minikube`

We need to choose our hardware virtualization software.
On macOS, these are the options for a driver: Docker, Hyperkit, VirtualBox, Parallels, VMware.

Now, let's start minikube with the preferred driver.

`minikube start --driver=parallels`

For future sake, we will set this as our default driver.

`minikube config set driver parallels`

Being that I have chosen Parallels we need additional software for it to work.

`brew install docker-machine-parallels`

Now that we have our hardware virtualization software ready, let's install kubectl.

`brew install kubectl`

### CI/CD Pick - Jenkins

I will be using helm to install Jenkins on our Kubernetes cluster.

But first, we need helm installed.

`brew install helm`

And the Jenkins helm repo.

`helm repo add jenkins https://charts.jenkins.io`<br>
`helm repo update`


Now that we have helm and the repo we can use it to install Jenkins with our values.

`helm install jenkins jenkins/jenkins -f values.yaml`

The first Jenkins being the name of our deployment.

After a few minutes, the Jenkins pod should be up and running.
But we still have no way of communicating with it.

In our values.yaml we have already configured the ingress config for Jenkins.

But we still need it enabled on our environment for it to work, so let's do that.

`minikube addons enable ingress`

After enabling ingress on our network, we need to check our minikube ip.

`minikube ip`

The username for jenkins is: admin <br>
We can obtain the password by running the following command:

`kubectl get secret --namespace default jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode`

In the browser, we can type http://[minikubeip] to access the Jenkins pod.

### AWS IAM - Credentials

In your AWS Console > IAM > Create new user<br>
With the following AWS access:
 - AmazonS3FullAccess

Copy the newly created credentials into your Jenkins Credentials with the name: jenkins.s3.access

### Global Tools - Setting up global node

In Manage Jenkins under Global Tool Configuration, we need to add a new tool.

Under NodeJs we add a new installer named "node" with a npm version: 14.15.0

### Builds - Creating the builds

In the base repository, you can find two files.
 - Jenkinsfile.dev
 - Jenkinsfile.prod
 
After creating two separate pipelines, each named appropriately.<br>
We can proceed to copy the contents of the Jenkins files into the Jenkinsfile field.

In the production build, under General tick, the "This project is parameterized".<br>
Under Name set: `build`<br>
Under Default Value: `branch.buildNumber`

### Installing MongoDB

Let us add the helm repo first

`helm repo add bitnami https://charts.bitnami.com/bitnami`

Now we can install MongoDB using helm

`helm install mongodb bitnami/mongodb`

### Deploying the k8s

Using kustomize we will deploy production and development deployments of the node app.

`kubectl apply -k ./kustomize/enviroments/prod`

`kubectl apply -k ./kustomize/enviroments/dev`

Now we can navigate to our minikubeip and test if the API is working

Open the following url http://[minikubeip]/dev/api

### Side Notes

In the kustomize/application/node-deployment.yaml the image is already set.<br>
If the Dockerfile needs to be modified, a new image needs to be built and pushed to the docker hub & the image field updated.
